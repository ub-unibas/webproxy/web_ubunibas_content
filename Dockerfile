FROM python:3.8

ENV FLASK_APP ub_web_scripts
ENV MODE prod

RUN useradd -m -s /bin/bash appuser

WORKDIR /
RUN mkdir data
RUN mkdir configs
RUN mkdir app
ADD setup.py /app
ADD requirements.txt /app

WORKDIR /app
RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /
ADD uwsgi_webcontent.py /app
ADD uwsgi_webcontent.ini /app
ADD ub_web_scripts app/ub_web_scripts
ADD k8s-manifests/configmap/gunicorn.conf.py /configs/
RUN git clone https://gitlab.switch.ch/ub-unibas/webproxy/searchbox-and-links.git /app/ub_web_scripts/static/searchbox-and-links

WORKDIR /app
EXPOSE 5000
#CMD ["/usr/local/bin/uwsgi", "uwsgi_webcontent.ini"]

ENTRYPOINT ["gunicorn"]
CMD ["uwsgi_webcontent:ub_webcontent_app", "--config", "/configs/gunicorn.conf.py"]
