# **UB Webscripts**

for content that cannnot be served as static html pages a flask service is used.
The service runs on ub_dynwebcontent (alias for ub-webproxy)
* runs as user web_services (group www-data)
* uses a python venv where the package needs to be installed ( ~/venv/bin/python3 setup.py install)
* is served via nginx/uwsgi (~/dyn_content/uwsgi_webcontent.)
* service ist called webContent

-> deployment folder

A Test Service exists:
* ub-test-dynwebcontent

## **Scripts**
* https://ub-dynwebcontent.ub.unibas.ch/check_accessrange: 
verifies if IP is within Uni access range (access to e-resources)
used on page: https://ub.unibas.ch/de/zugangsberechtigung/
* https://ub-dynwebcontent.ub.unibas.ch/redirect:
used in nginx webproxy configuration to rewrite old links to new links (defined in settings/redirects.xlsx) 
* https://ub-dynwebcontent.ub.unibas.ch/redirect_rules:
show redirect rules from settings/redirects.xlsx file
* https://ub-dynwebcontent.ub.unibas.ch/latest_blog_entry:
small iframe to embed link to UB Blog
* https://ub-dynwebcontent.ub.unibas.ch/maxi:
show how many persons are within a library building
* https://ub-dynwebcontent.ub.unibas.ch/location/<kuerzel>:
green/yellow/red lights if there are too many persons in a building
* https://ub-dynwebcontent.ub.unibas.ch/neuerwerb/<type>:
type = fach/bib: show neuerwerbungslisten based on google spreadsheet values

# Spreadsheet Zugriff
Service Account für ubunibas@gmail.com
APIS aktivieren: Drive, DOcs, Sheet
