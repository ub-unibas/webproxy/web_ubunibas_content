import socket

HOSTNAME = socket.gethostname()

if __name__ == "__main__":
    from ub_web_scripts.web_scripts import ub_webcontent_app, save_maxi_report
    ub_webcontent_app.run(port=int("5051"), debug=True, ssl_context="adhoc")


