#!/usr/bin/python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="web_ubunibas_content",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="dynamic iframes for ub webpage",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.switch.ch/ub-unibas/webproxy/web_ubunibas_content.git",
    install_requires=['requests', 'flask', 'gdspreadsheets_ubit', 'cache_decorator_redis_ubit', 'pyyaml',  'flask_wtf', 'wtforms', 'flask_cors',
                      'flask_compress', 'uwsgi', 'netaddr', 'openpyxl', 'mysql-connector', 'feedparser', 'elasticsearch<8', 'rdv_data_helpers_ubit'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"": "ubunibas_loggers"},
    packages=setuptools.find_packages(),
    package_data={
        '': ['*'],
    },
    python_requires=">=3.8",
    include_package_data=True,
    zip_safe=False
)