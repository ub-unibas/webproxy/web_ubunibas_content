def build_link_from_config(line):
    if line["fachgebiet"] == "Bibliothek Maiengasse":
        return "<a class=\"external-link\" href=\"https://bibliomaiengasse.philhist.unibas.ch/de/services/neuerwerbungen/\" " \
               "target=\"_self\">{}</a>".format("Bibliothek Maiengasse")
    link_template = "<a class=\"external-link\" href=\"javascript:fnel('{}')\" target=\"_self\">{}</a>"
    # add leading zeros to ddc_codes
    line["ddc_codes"] = "|".join([(3 -len(v)) * "0" + v for v in line.get("ddc_codes","").split("|") if v])
    query_str = "+".join([v for k in ["ddc_codes", "fachcodes", "bib_codes"] for v in line.get(k,"").split("|") if v])
    link = link_template.format(query_str, line.get("fachgebiet"))
    return link

def build_neuerwerb_links(neuerwerbungs_def):
    prev_fachgebiet = ""
    all_fachgebiete = ""
    for entry in neuerwerbungs_def:
        if entry:
            fachgebiet = entry.get("fachgebiet")
            if prev_fachgebiet and prev_fachgebiet[0] != fachgebiet[0]:
                prev_fachgebiet=fachgebiet
                all_fachgebiete += "</br></br>" + build_link_from_config(entry)
            elif prev_fachgebiet:
                prev_fachgebiet = fachgebiet
                all_fachgebiete += "</br>" + build_link_from_config(entry)
            else:
                prev_fachgebiet = fachgebiet
                all_fachgebiete += build_link_from_config(entry)
    return all_fachgebiete