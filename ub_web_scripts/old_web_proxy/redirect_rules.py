import openpyxl

def get_xlsxdict(file, sheet, key_field):
    """get lookup from xlsx file"""
    try:
        workbook = openpyxl.load_workbook(file, data_only=True)
        sheet = workbook["redirect"]
    except IOError as e:
        import traceback
        traceback.print_exc()
        print("Input file {} or sheet {} does not exist {}".format(file, "redirect", e))

    rows = sheet.rows
    keys = [c.value for c in next(rows)]
    sheet_values_dict = {}

    for row in rows:
        values = [c.value for c in row]
        row_dict = dict(zip(keys, values))
        id_ = row_dict.get(key_field).strip("/")
        sheet_values_dict[id_] = row_dict
    return sheet_values_dict