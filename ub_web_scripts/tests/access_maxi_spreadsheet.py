import pygsheets

from gdspreadsheets_ubit import PygExtendedClient,authorize_pyg

service_file = "/home/martin/PycharmProjects/web_ubunibas_content/personenzaehlanlage-79993bb25b5c.json"
spreadsheet_id = "1MbOuNcOjpK7JqoQGhQVTxslm2Mruyzg3AeWteNaLrvQ"
sheet = "maxBelegung"
key_field = "kuerzel"

PygExtendedClient.gc = authorize_pyg(service_file=service_file)
max_sheet = PygExtendedClient.gc.open_by_key(spreadsheet_id)
max_sheet =max_sheet.worksheet_by_title(sheet)
print(max_sheet.gSpreadsheetKeyDict(key_field))