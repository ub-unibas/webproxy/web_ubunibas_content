import os
import socket
import pickle
import sqlite3
from ub_web_scripts.ub_alephauth import BestellForm
MODE = os.getenv('MODE') or "debug"
DEBUG = True if MODE == "debug" else False

# Legacy Auth for Aleph not needed anymore

from flask_login import LoginManager

login_manager = LoginManager()
#login_manager.init_app(ub_webcontent_app)
login_manager.login_view = "login"

if socket.gethostname() in ["reisache"]:
    db_name = '/tmp/auth_aleph.db'
    backup_dir = "/tmp"
# wird nicht erkannt, da nur Alias
elif socket.gethostname() in ["ub-test-webform"] or MODE == "test":
    db_name = '/tmp/auth_aleph.db'
    backup_dir = "/home/web_services/delay_archiv/test"
else:
    DEBUG = False
    db_name = '/tmp/auth_aleph.db'
    backup_dir = "/home/web_services/delay_archive/prod"

try:
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute("CREATE TABLE auth_users (uid text primary key, user text)")
    conn.commit()
    conn.close()
except Exception:
    pass

@login_manager.user_loader
def user_loader(uid):
    conn = sqlite3.connect('/tmp/auth.db')
    c = conn.cursor()
    c.execute('SELECT * FROM auth_users WHERE uid=?', [uid])
    db_user = c.fetchone()
    conn.close()
    if db_user:
        pickled_user = db_user[1]
        # AlephAuth.auth_users.get(uid)
        user = pickle.loads(pickled_user)
        if user and uid:
            return user
        else:
            return None
    else:
        return None

# aleph auth
@ub_webcontent_app.route('/delay_form', methods=['GET', 'POST'])
def delay_form():
    return BestellForm.form_auth_template(request, user= None, remove_empty_fields=False, header=False,
                                                               mode=MODE, backup_dir=backup_dir)