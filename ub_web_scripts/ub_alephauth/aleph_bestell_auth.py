import requests
import datetime
from xml.etree import ElementTree

from flask_login import UserMixin
from flask_wtf import FlaskForm

from wtforms import StringField, PasswordField, BooleanField, SubmitField, validators, TextAreaField, HiddenField, ValidationError
from flask import render_template, redirect
from wtforms.widgets import PasswordInput
import pickle
import sqlite3
import smtplib
from email.message import EmailMessage

# Legacy Auth for Aleph not needed anymore

def check_aleph(form, field):
    user = AlephBestellAuth(field.data, form.password_hidden.data)
    print(user)
    if user.is_authenticated:
        pass
    else:
        raise ValidationError(
            'Ihre Anmeldedaten stimmen nicht für {}.'.format(field.data))


class AlephBestellAuth(UserMixin):


    def __init__(self, uid, pwd, host="aleph.unibas.ch", library="dsv51"):
        self.uid = uid
        self.pwd = pwd
        self.url = "http://{}/X?op=bor-auth&library={}&bor_id={}" \
                   "&verification={}".format(host, library, uid, pwd)
        self.xml_login_response =  self.query_aleph() #ElementTree.fromstring(open("/home/martin/PycharmProjects/auth/test.xml").read()) #

    def query_aleph(self):
        try:
            xml_data = requests.get(self.url).text
            xml_login_response = ElementTree.fromstring(xml_data)
        except Exception:
            import traceback
            traceback.print_exc()
            xml_login_response = ""
        return xml_login_response

    def get_id2(self):
        conn = sqlite3.connect('/tmp/auth.db')
        c = conn.cursor()
        c.execute('SELECT * FROM auth_users WHERE uid=?', [self.uid])
        db_user = c.fetchone()
        conn.close()
        if db_user:
            pickled_user = db_user[1]
            # AlephAuth.auth_users.get(uid)
            user = pickle.loads(pickled_user)
            if user:
                return self.uid

    @property
    def is_anonymous(self):
        if not self.uid:
            return True

    @property
    def is_authenticated(self):
        if self.xml_login_response and self.is_active:

            # print(pickeled_auth_user)
            if not self.get_id():
                conn = sqlite3.connect('/tmp/auth.db')
                c = conn.cursor()
                pickeled_auth_user = pickle.dumps(self)
                c.execute("INSERT INTO auth_users VALUES (?, ?)", [self.uid, pickeled_auth_user])
                conn.commit()
                conn.close()
            return True
        else:
            return False

    @property
    def user_data(self):
        if self.xml_login_response:
            print(self.xml_login_response, self.xml_login_response.findall("z303"))
            for z303 in self.xml_login_response.findall("z303") or []:
                try:
                    slsp_full_name = z303.find("z303-name").text
                except Exception:
                    slsp_full_name = "nicht angegeben"
                try:
                    slsp_birthdate = z303.find("z303-birth-date").text
                except Exception:
                    slsp_birthdate = "nicht angegeben"
            for z304 in self.xml_login_response.findall("z304") or []:
                try:
                    slsp_mail = z304.find("z304-email-address").text
                except Exception:
                    slsp_mail = "nicht angegeben"
            return {"slsp_full_name": slsp_full_name, "slsp_birthdate": slsp_birthdate, "slsp_mail": slsp_mail}
        else:
            return False

    @property
    def is_active(self):
        if self.xml_login_response:
            for z305 in self.xml_login_response.findall("z305") or []:
                expiry = z305.find("z305-expiry-date").text
                sub_lib = z305.find("z305-sub-library").text
                expiry_date = datetime.datetime.strptime(expiry, "%d/%m/%Y")
                # delinquency note?
                if expiry_date > datetime.datetime.now():
                    self.id = self.uid
                    return True
                else:
                    return False
        else:
            return False


class BestellForm(FlaskForm):
    form_title = "Aufschub Formular"
    smtp_host = "smtp.unibas.ch"

    username = StringField('Bisherige Benutzungsnummer (swissbib Basel Bern) z.B. A46155', [validators.required(message="Feld muss ausgefüllt sein"), check_aleph])
    password_hidden = StringField('Passwort bisherige Nummer', [validators.required(message="Feld muss ausgefüllt sein")], widget=PasswordInput(hide_value=False))
    benutzernummer = StringField('Benutzungsname swisscovery / SWITCH edu-ID z.B. felix.muster@hotmail.com',
                                  [validators.required(message="Feld muss ausgefüllt sein")])
    kommentar = TextAreaField('Begründung', [validators.required(message="Feld muss ausgefüllt sein")],
                              render_kw={'class': 'text-field'})

    slsp_full_name = HiddenField("Name")
    slsp_birthdate = HiddenField("Geburtsdatum")
    slsp_mail = HiddenField("Email")

    submit = SubmitField('Weiter')
    confirm = SubmitField('Bestätigen')
    back = SubmitField('Zurück')
    reload = SubmitField('Reload')
    new = SubmitField('Neues Formular')

    template = "aleph_auth/auth_form_temp.html"
    specific_parts = None

    @classmethod
    def form_auth_template(cls, request, user, remove_empty_fields=False, header=False, mode="debug", backup_dir=None):
        try:

            self = cls(request.form)
            self.backup_dir = backup_dir
            form_title = "{} {}".format(mode.upper(), cls.form_title) if mode != "prod" else cls.form_title
            from collections import OrderedDict
            request_args = OrderedDict()
            from copy import deepcopy


            if request.method == 'POST' and (self.confirm.data or self.submit.data) and self.validate():
                update_args = deepcopy(request_args)
                uid = request.form['username']
                pwd = request.form['password_hidden']
                al = AlephBestellAuth(uid, pwd)
                aleph_user = al.user_data
                update_args.update(aleph_user)
                if self.confirm.data:
                    # for user data
                    self.process(formdata=request.form, obj={}, **aleph_user)
                    self.send_formdata_mail(request, mode, aleph_user)
                    return render_template(cls.template, title=form_title,
                                           form=self, eduid_service=True, user=user,
                                           remove_empty_fields=remove_empty_fields, status="finished", header=header,
                                           specific_parts=self.specific_parts)
                # confirm entries
                elif self.submit.data:
                    # for user data
                    self.process(formdata=request.form, obj={}, **aleph_user)
                    return render_template(cls.template, title=form_title,
                                           form=self, eduid_service=True, user=user,
                                           remove_empty_fields=remove_empty_fields, status="confirm", header=header,
                                           specific_parts=self.specific_parts)
            elif self.new.data:
                return redirect(request.script_root + request.path)
            else:
                return render_template(cls.template, title=form_title,
                                       form=self, eduid_service=True, user=user,
                                       remove_empty_fields=remove_empty_fields, status="fillout", header=header,
                                       specific_parts=self.specific_parts)

        except Exception as e:
            import traceback
            traceback.print_exc()

            return render_template(template_name_or_list="aleph_auth/message_template.html", message=str(e.args),
                                   header=False, error=True)



    def get_form_lookups(self):

        field_names = {field.id: field.label.text for field in
                       self if field.type not in ["SubmitField", "CSRFTokenField"] and field.id != 'man_benutzernummer' }
        value_lookup = {}
        for field in self:
            if field.type in ["SelectField", "RadioField"]:
                key = field.label.field_id
                value_lookup.setdefault(key, {})
                for subkey in field.choices:
                    value_lookup[key][subkey[0]] = subkey[1]
        return field_names, value_lookup

    def build_mailcontent(self, add_content, request):
        field_names, value_lookup = self.get_form_lookups()
        mailcontent = ""
        form_values = {}
        for key, val in add_content.items():
            # {:.<40}
            if key in field_names or key in ["Bestelldatum", "Auftragsnummer"]:
                if not key.endswith("_hidden"):
                    form_values[key] = val
                    if value_lookup.get(key):
                        val = value_lookup.get(key).get(val)
                    if val:
                        mailcontent += "{}: {}\n".format(field_names.get(key, key), val)
            form_values[key] = val
        mailcontent += "\n"
        for key, val in request.form.items():
            if key in field_names:
                if not key.endswith("_hidden"):
                    form_values[key] = val
                    if value_lookup.get(key):
                        val = value_lookup.get(key).get(val)
                    mailcontent += "{}: {}\n".format(field_names.get(key, key), val)
        return mailcontent, form_values

    def send_formdata_mail(self, request, mode="debug", update_args={}):
        add_content = update_args
        add_content["Ausfülldatum"] = datetime.datetime.now().strftime("%d.%m.%Y, %H:%M:%S")
        mailcontent, form_values = self.build_mailcontent(add_content, request)
        form_values.update(add_content)
        self.send_mail(mailcontent, form_values, mode=mode, mail_from=update_args.get("slsp_mail", "Nicht angegeben"), mail_to="ylp-ub@unibas.ch")

    def send_mail(self, mailcontent, form_values, mode="debug", mail_from=None, mail_to=None):
        """
        $mailtext = $u2a->convert($mailtext);
        $Mail::Sender::NO_X_MAILER = 1;
                charset =>  'ISO-8859-1',
            encoding => '8BIT',
        """

        msg = self.get_ticket_msg(mailcontent, mode=mode, mail_from=mail_from, mail_to=mail_to)
        msg_customer = self.get_customer_msg(mailcontent, mode=mode, mail_to=mail_from)

        if mode == "debug":
            print(mailcontent)
            print(msg)
        else:
            s = smtplib.SMTP(self.smtp_host)
            s.send_message(msg)
            s.send_message(msg_customer)
            s.quit()

    def get_ticket_msg(self, mailcontent, mode="debug", mail_from=None, mail_to=None):
        """
        $mailtext = $u2a->convert($mailtext);
        $Mail::Sender::NO_X_MAILER = 1;
                charset =>  'ISO-8859-1',
            encoding => '8BIT',
        """
        msg = EmailMessage()
        msg.set_content(mailcontent)
        msg['Subject'] = "{} Ticket Aufschub Mahngebühren Aleph".format(mode.upper() + " " if mode != "prod" else "")
        msg['From'] = mail_from
        msg['To'] = mail_from if mode != "prod" else mail_to
        return msg

    def get_customer_msg(self, mailcontent, mode="debug", mail_to=None):
        """
        $mailtext = $u2a->convert($mailtext);
        $Mail::Sender::NO_X_MAILER = 1;
                charset =>  'ISO-8859-1',
            encoding => '8BIT',
        """
        msg = EmailMessage()
        msg.set_content(mailcontent)
        msg['Subject'] = "{} Bestätigung Antrag Aufschub Mahngebühren Aleph".format(mode.upper() + " " if mode != "prod" else "")
        msg['To'] = mail_to
        msg['From'] = mail_to if mode != "prod" else "ylp-ub@unibas.ch"
        return msg

