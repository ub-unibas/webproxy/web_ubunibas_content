import os
import json
import time
import traceback

from flask import Flask, make_response, jsonify, render_template, request, redirect
from netaddr import IPNetwork, IPAddress
from pkg_resources import Requirement, resource_filename
from threading import Thread

from elasticsearch import helpers, Elasticsearch

from ub_web_scripts.web_settings import websettings

MODE = os.getenv('MODE') or "debug"
MAIL = websettings.global_settings["mail"]
ub_webcontent_app = Flask(__name__)
# ub_webcontent_app.config["EXPLAIN_TEMPLATE_LOADING"]=True

# needs to be static for uwsgi
ub_webcontent_app.secret_key = os.getenv('WEBCONTENT_FLASK_SECRET_KEY')

def save_maxi_report():
    while 1:
        try:
            json_file = websettings.maxi_ampel["counter_file"]
            counter = json.load(open(json_file))
            new_data = ""

            # for first run
            try:
                current_time
            except UnboundLocalError:
                current_time = ""

            new_time = counter.get("meta", {}).get("current_time")
            if new_time != current_time:
                current_date = counter.get("meta", {}).get("current_date")
                current_time = counter.get("meta", {}).get("current_time")
                parts = []
                if os.path.exists(websettings.maxi_ampel["out_file"]) and os.path.getsize(websettings.maxi_ampel["out_file"]) != 0:
                    out_read = open(websettings.maxi_ampel["out_file"], "rb")
                    out_read.seek(-2, os.SEEK_END)
                    while out_read.read(1) != b'\n':
                        out_read.seek(-2, os.SEEK_CUR)
                    last_line = out_read.readline().decode()
                    out_read.close()
                    parts = last_line.split(" ")
                if not parts:
                    parts = [None, None]
                if current_time != parts[1]:
                    for k, values in counter["data"].items():
                        people = counter["data"][k]["number_of_people"]
                        new_data += "{} {} {} {}\n".format(current_date, current_time, k, people)
                    out_fh = open(websettings.maxi_ampel["out_file"], "a")
                    out_fh.write(new_data)
                    out_fh.close()
            time.sleep(360)
        except Exception:
            pass


#only for test-mode
if MODE == "test":
    Thread(target=save_maxi_report).start()


import pygsheets
from gdspreadsheets_ubit import PygExtendedClient, authorize_pyg
from cache_decorator_redis_ubit import  SettingsShortCacheDecorator, SettingsCacheDecorator12h
cache_decorator = SettingsCacheDecorator12h if MODE == "prod" else SettingsShortCacheDecorator

# set to 10 seconds so not every uwsgi process needs to access Spreadsheets
PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var='GOOGLE_DOC_API_KEY',
                                     service_file=websettings.maxi_ampel["gd_service_file"],
                                     cache_decorator=cache_decorator)
spreadsheet = PygExtendedClient.gc.open_by_key(websettings.maxi_ampel["spreadsheet_id"])
max_sheet = spreadsheet.worksheet_by_title(websettings.maxi_ampel["sheet"])

# uses same service file as maxi
neuerwerb_spreadsheet = PygExtendedClient.gc.open_by_key(websettings.neuerwerb["spreadsheet_id"])
neuerwerb_fach_sheet = neuerwerb_spreadsheet.worksheet_by_title(websettings.neuerwerb["fach_sheet"])
neuerwerb_bib_sheet = neuerwerb_spreadsheet.worksheet_by_title(websettings.neuerwerb["bib_sheet"])

@ub_webcontent_app.route("/neuerwerb/<neuerwerb_type>", methods=["GET"], defaults={"lang": "de"})
@ub_webcontent_app.route('/neuerwerb/<neuerwerb_type>/<lang>')
def neuerwerb_html(neuerwerb_type, lang):
    from ub_web_scripts.neuerwerb import build_neuerwerb_links
    try:
        if neuerwerb_type == "bib":
            neuerwerb_data = neuerwerb_bib_sheet.gSpreadsheetsDictReader()
        else:
            neuerwerb_data = neuerwerb_fach_sheet.gSpreadsheetsDictReader()
    except Exception as e:
        import traceback
        traceback.print_exc()
        return("Can't access spreadsheet {}, using fallback values, exception: {}".format(
            websettings.neuerwerb["spreadsheet_id"], e))
    neuerwerb_body = build_neuerwerb_links(neuerwerb_data)

    return render_template('neuerwerb/neuerwerb.html', neuerwerb_body=neuerwerb_body)


def filter_json_counter_data(locations_max_dict):
    json_file= websettings.maxi_ampel["counter_file"]
    with open(json_file) as counter_file:
        counter = json.load(counter_file)
        # defined here because no access to server skript

        filtered_counter = {}
        for k, values in locations_max_dict.items():
            filtered_counter[k] = counter["data"][k]
            try:
                filtered_counter[k]["max_people"] = int(values["max"])
            except ValueError:
                filtered_counter[k]["max_people"] = 0
            if values.get("name"):
                filtered_counter[k]["name"] = values.get("name")

        current_date = counter.get("meta", {}).get("current_date")
        current_time = counter.get("meta", {}).get("current_time")
        return filtered_counter, current_date, current_time

@ub_webcontent_app.route("/maxi", methods=["GET"], defaults={"lang": "de"})
@ub_webcontent_app.route('/maxi/<lang>')
def maxi(lang):
    try:
        locations_max = max_sheet.gSpreadsheetKeyDict(websettings.maxi_ampel["key_field"])

    except Exception as e:
        import traceback
        traceback.print_exc()
        print("Can't access spreadsheet {}, using fallback values, exception: {}".format(
            websettings.maxi_ampel["spreadsheet_id"], e))
        locations_max = {"ubh": {"max": 262}, "ubhl": {"max": 164}, "ubm": {"max": 114},
                         "ubw": {"max": 120}, "jus": {"max": 150}, "rel": {"max": 30}, "ros": {"max": 70},
                         "baw": {"max": 80, "name": "BAW Rosshof"},
                         "bam": {"max": 65, "name": "Bibliothek Maiengasse"}}
    filtered_counter, current_date, current_time = filter_json_counter_data(locations_max)
    return render_template('maxi_ampel/all_lights.html', count_date=current_date, count_time=current_time,
                           counters=filtered_counter, lang=lang)

@ub_webcontent_app.route('/location/<name>')
def location(name):
    try:
        locations_max = max_sheet.gSpreadsheetKeyDict(websettings.maxi_ampel["key_field"])
    except Exception as e:
        import traceback
        traceback.print_exc()
        print("Can't access spreadsheet {}, using fallback values, exception: {}".format(
            websettings.maxi_ampel["spreadsheet_id"], e))
        locations_max = {"ubh": {"max": 262}, "ubhl": {"max": 164}, "ubm": {"max": 114},
                         "ubw": {"max": 120}, "jus": {"max": 150}, "rel": {"max": 30}, "ros": {"max": 70},
                         "baw": {"max": 80, "name": "BAW Rosshof"},
                         "bam": {"max": 65, "name": "Bibliothek Maiengasse"}}
    filtered_counter, current_date, current_time = filter_json_counter_data(locations_max)
    return render_template('maxi_ampel/location.html', name=name, count_date=current_date, count_time=current_time,
                           counters=filtered_counter)

@ub_webcontent_app.route('/_get_counter_data/<name>')
def get_counter_data(name):
    json_file= websettings.maxi_ampel["counter_file"]
    with open(json_file) as counter_file:
        counter_data = json.load(counter_file)
        counters = counter_data["data"].get(name, 0)
        return jsonify(counters=counters)


# Access Range
@ub_webcontent_app.route("/check_accessrange", methods=["GET"], defaults={"lang": "de"})
@ub_webcontent_app.route("/check_accessrange/<lang>", methods=["GET"])
def get_my_ip(lang):
    access_message_tpl = "Access to licensed e-media possible (IP: {})" \
        if lang == "en" else "Zugriff auf lizenzierte E-Medien möglich (IP: {})"
    no_access_message_tpl = "No access to licensed e-media (IP: {})" \
        if lang == "en" else "Kein Zugriff auf lizenzierte E-Medien (IP: {})"
    try:
        IP_RANGES = websettings.check_accessrange["uni_range"]
        ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
        for ip_range in IP_RANGES:
            if IPAddress(ip) in IPNetwork(ip_range):
                message = access_message_tpl.format(ip)
                html = render_template("access_range/access_range.html", message=message, color="green", icon="fas fa-check")
                break
        else:
            message = no_access_message_tpl.format(ip)
            html = render_template("access_range/access_range.html", message=message, color="red", icon="fas fa-times")
        return html, 200
    except Exception as e:
        return "Accessrange Endpoint not working {}. Contact {}".format(e, MAIL), 500


@ub_webcontent_app.route("/latest_blog_entry", methods=["GET"], defaults={"pos": "0"})
@ub_webcontent_app.route("/latest_blog_entry/<pos>", methods=["GET"])
def get_latest_blog_entry(pos):
    import feedparser
    from flask import render_template
    url = "https://blog.ub.unibas.ch/feed/"
    rss_feed = feedparser.parse(url)
    try:
        pos = int(pos)
    except TypeError:
        pos = 0
    latest_entry = rss_feed.entries[pos]
    title = latest_entry.title
    image = latest_entry.media_thumbnail[0].get("url")
    link = latest_entry.links[0].get("href")

    html = render_template("ubblog/blog_snippet_new.html", title=title, image=image, url=link)
    return html, 200

# Webproxy
try:
    from ub_web_scripts.old_web_proxy import get_xlsxdict
    redirect_def_file = os.path.join(websettings.webproxy["resolve_file"])
    PATH_LOOKUP = get_xlsxdict(file=redirect_def_file, sheet=websettings.webproxy["sheet"], key_field=websettings.webproxy["resolve_key"])
except Exception as e:
    traceback.print_exc()
    print("Redirect to old web page not working {}".format(e, MAIL))


# Webproxy ITB / Sachdok
try:
    es = Elasticsearch(websettings.webproxy["es_host"])

    ITB_LOOKUP = {}
    SACHDOK_LOOKUP = {}
    SACHDOK_PDF_LOOKUP = {}

    itb_url_prefix = "https://ub-itb.ub.unibas.ch/de/detail/{}"
    for result in helpers.scan(es, index=websettings.webproxy["itb_index"]):
        real_url = result.get("_source").get("real_url")
        drucker = real_url.split("bbk/druckerverleger/")[-1]
        drucker = drucker.strip("/")
        new_url = itb_url_prefix.format(result.get("_id"))
        ITB_LOOKUP[drucker] = new_url

    sachdok_url_prefix = "https://ub-swasearch-test.ub.unibas.ch/de/detail/{}"
    #sachdok_url_prefix = "http://localhost:4208/de/detail/{}"
    from rdv_data_helpers_ubit import IZID_ESFIELD, OLDSYS_ESFIELD
    # from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import PDF_LINKS, OLD_PDF_FILEPATHS, SACHDOK_INDEX, SACHDOK_TEST_INDEX
    PDF_LINKS = "swa_archive_links"
    OLD_PDF_FILEPATHS = "pdf_old_filepaths"
    SACHDOK_INDEX = "swasearch"
    SACHDOK_TEST_INDEX = "swapdfs"
    n = 0

    for result in helpers.scan(es, index=SACHDOK_INDEX, query={"query":{"exists": {"field": PDF_LINKS}}}):
        n +=1
        page_sysid = (result.get("_source").get(OLDSYS_ESFIELD, "") or "").split("_")[-1]
        bsiz_id = result.get("_source").get(IZID_ESFIELD)
        new_url = sachdok_url_prefix.format(result.get("_id"))
        if page_sysid:
            SACHDOK_LOOKUP[page_sysid] = new_url
        if bsiz_id:
            SACHDOK_LOOKUP[bsiz_id] = new_url

    #sachdok_url_prefix = "http://localhost:4208/de/detail/{}"

    for result in helpers.scan(es, index=SACHDOK_INDEX, query={"query":{"exists": {"field": PDF_LINKS}}}):
        # auf array umschreiben: OLD_PDF_FILEPATHS
        pdfs = result.get("_source").get(OLD_PDF_FILEPATHS,[])
        new_url = sachdok_url_prefix.format(result.get("_id"))
        if isinstance(pdfs, list):
            for pdf in pdfs:
                old_path = pdf.split("/a125/sachdok/")[-1]
                if old_path and not old_path in SACHDOK_PDF_LOOKUP:
                    SACHDOK_PDF_LOOKUP[old_path] = new_url
        else:
            old_path = pdfs.split("/a125/sachdok/")[-1]
            if old_path:
                SACHDOK_PDF_LOOKUP[old_path] = new_url

except Exception as e:
    traceback.print_exc()
    print("Redirect to old web page not working {}".format(e, MAIL))

# alte ITB URL: "http://www.ub.unibas.ch/bbk/druckerverleger/jacob-christoph-im-hof"
# alte Sachok URL: "https://ub.unibas.ch/ub-wirtschaft-swa/swadok/005320736/"
# alte Sachdok URL: "https://ub.unibas.ch/ub-wirtschaft-swa/swadokjb/00664791/"
@ub_webcontent_app.route("/ub-wirtschaft-swa/swadok/<page_id>/", methods=["GET"])
@ub_webcontent_app.route("/ub-wirtschaft-swa/swadokjb/<page_id>/", methods=["GET"])
@ub_webcontent_app.route("/swadok/<page_id>/", methods=["GET"])
@ub_webcontent_app.route("/bbk/druckerverleger/<page_id>/", methods=["GET"])
@ub_webcontent_app.route("/itb/druckerverleger/<page_id>/", methods=["GET"])
@ub_webcontent_app.route("/digi/a125/sachdok/<path:page_id>/", methods=["GET"])
def redirect_swa_itb(page_id):
    try:
        old_path = page_id.strip("/")

        redirect_url = SACHDOK_LOOKUP.get(old_path) or ITB_LOOKUP.get(old_path) or SACHDOK_PDF_LOOKUP.get(old_path)
        print(redirect_url)
        if redirect_url:
            return redirect(redirect_url, code=301)
        else:
            return redirect("https://ub.unibas.ch/de", code=301)
    except Exception as e:
        traceback.print_exc()
        return "ITB/Sachdok redirect not working {}. Contact {}".format(e, "ub-it@unibas.ch"), 500

@ub_webcontent_app.route("/redirect_rules", methods=["GET"])
def redirect_rules():
    try:
        redirects = "REWRITE RULES \n\n"
        for k, v in PATH_LOOKUP.items():
            redirect_url = v.get("new_address")
            redirects += "{} -> {}\n".format(k, redirect_url)
        response = make_response(redirects, 200)
        response.mimetype = "text/plain"
        return response
    except Exception as e:
        traceback.print_exc()
        return "Redirect Rules not working {}. Contact {}".format(e), 500

@ub_webcontent_app.route("/<path:old_path>", methods=["GET"])
def redirect_request(old_path):
    # TODO_ redirect rules für typo3 für ITB und sachdok aus ES auslesen, über page-id und über sys-id mit und ohne nullen
    try:
        old_path = old_path.strip("/")
        redirect_url = PATH_LOOKUP.get(old_path, {}).get("new_address")
        if redirect_url:
            return redirect(redirect_url, code=301)
        else:
            if old_path.startswith("ub-medizin"):
                return redirect("https://ub.unibas.ch/de/standorte/ub-medizin", code=301)
            elif old_path.startswith("ub-wirtschaft-swa"):
                return redirect("https://ub.unibas.ch/de/ub-wirtschaft-swa/", code=301)
            elif old_path.startswith("ub-hauptbibliothek"):
                return redirect("https://ub.unibas.ch/de", code=301)
            else:
                return redirect("https://ub.unibas.ch/de", code=301)
    except Exception as e:
        traceback.print_exc()
        return "Redirect not working {}. Contact {}".format(e, "ub-it@unibas.ch"), 500




@ub_webcontent_app.route("/iiifwall/<wallname>", methods=["GET"])
def build_iiif_wall(wallname):
    def build_div(entry):
        viewer_dict = {"uv": "https://universalviewer.io/uv.html?manifest=",
                       "mirador": "https://ub-mirador.ub.unibas.ch?manifest="}
        viewer = viewer_dict.get(entry.get("Viewer"), viewer_dict.get("mirador"))
        # image = entry.get("Bildlink", "").replace("https://", "http://")
        image = entry.get("Bildlink", "")
        image_parts = image.split("/")
        image_parts_full = image_parts[:]

        image_parts[-1] = "gray.jpg"
        image_parts_full[-3] = "1500,"
        image = "/".join(image_parts)

        image_parts_full[-4] = "full"
        image_parts_full[-3] = "1500,"
        image_parts_full[-1] = "default.jpg"

        full_image = "/".join(image_parts_full)

        return "<div class='cell'><a href='" + viewer + entry.get("Manifestlink", "") + \
               "'>" "<div class='images'><img class='card' src='" + image + \
               "'/>" "<img class='card_back' src='" + full_image + "'></div></a></div>\n"


    def cache_iiifwall(wallname):
        css = """
        img {
            object-fit: cover;
            width: 100%;
            height: 100%;
        }
        .images {
            width: 100%;
            height: 100%;
            display: inline-block;
            position: relative;
            box-sizing: border-box;
            transition: opacity 2s ease-in-out;
        }
        .images .card_back {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            z-index: 99;
            #box-sizing: border-box;
        }
        .images:hover .card_back{
            display: inline;
            #border: 4px solid;
            #color: #a5d7d2;
        }
        .cell {
            display: block;
            width: ###sqrt###;
            height: 100%;
            float: right;
        }
        .row {
            display: block;
            height: ###sqrt###;
            width: 100%;
            float: right;
        }
        html,body {
            height: 100%;
            width: 100%;
        }
        body {margin: 0px}
        """

        iiif_wall_dict = {}
        import math
        # set to 10 seconds so not every uwsgi process needs to access Spreadsheets
        PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var='GOOGLE_DOC_API_KEY',
                                             service_file=websettings.maxi_ampel["gd_service_file"],
                                             cache_decorator=cache_decorator)
        spreadsheet_key = "1rnzuckKxEFQwLHj61THj93sgshAZgPTXZg4wbCV-gJE"
        spreadsheet = PygExtendedClient.gc.open_by_key(spreadsheet_key)

        for sheet in spreadsheet.worksheets(sheet_property="title", value=wallname):
            data_dict = sheet.get_all_records()
            data_dict = list(filter(lambda x: x.get("Bildlink", ""), data_dict))
            number_entries = len(data_dict)
            wurzel = int(math.sqrt(number_entries))
            if number_entries:
                css = css.replace("###sqrt###", str(100 / wurzel) + "%")
                page = "<html><head><style>" + css + "</style></head><body>"
                for n, _entry in enumerate(data_dict[0:wurzel * wurzel:wurzel]):
                    page += "<div class='row'>\n"
                    for x, entry in enumerate(data_dict[n * wurzel:n * wurzel + wurzel]):
                        page += build_div(entry)
                    page += "</div>\n"
                page += "</body></html>"
                iiif_wall_dict[wallname] = page
        return iiif_wall_dict[wallname]

    return cache_iiifwall(wallname)

