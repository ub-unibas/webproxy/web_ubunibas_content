import os
import sys
import copy
import configparser

from pkg_resources import Requirement, resource_filename

MODE = os.getenv('MODE') or "debug"
print("MODE", MODE)

class WebSettings():

    def __init__(self, ini_file: str) -> None:
        """load settings from ini file into settings object for easier access

        :param ini_file: path for project ini file
        :param dataset_type: type of dataset: pub, proj, org, person, ach
        """

        try:
            self.config = configparser.ConfigParser()
            self.config.read(ini_file)
            print("{} loaded".format(ini_file))
        except Exception as e:
            print("Settings {} does not exist: {}".format(ini_file, e))
            sys.exit(1)

        try:
            # dict containing cache settings
            self.global_settings = dict(self.config["global"])
            self.check_accessrange = self.build_settings("check_accessrange")
            self.check_accessrange["uni_range"] = self.build_list(self.check_accessrange["uni_range"])
            self.webproxy = self.build_settings("webproxy")
            self.maxi_ampel = self.build_settings("maxi_ampel")
            self.neuerwerb = self.build_settings("neuerwerb")
        except Exception as e:
            import traceback
            traceback.print_exc()
            print("Problem loading config file: {} : {}".format(ini_file, e))

    def build_list(self, value, split_char=","):
        values = [v.strip() for v in value.split(split_char)]
        return values

    def build_settings(self, key):
        settings = copy.deepcopy(self.global_settings)
        settings.update(dict(self.config[key]))
        return settings

if MODE in ["debug"]:
    ini_file = os.path.join("ub_web_scripts", "ini", "local.ini")
elif MODE in ["prod"]:
    ini_file = os.path.join("ub_web_scripts", "ini", "dyncontent.ini")
elif MODE in ["test"]:
    ini_file = os.path.join("ub_web_scripts", "ini", "dyntestcontent.ini")


print(ini_file)
websettings = WebSettings(ini_file)
