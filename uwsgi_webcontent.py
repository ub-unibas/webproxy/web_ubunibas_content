from ub_web_scripts import ub_webcontent_app
import logging
from flask.logging import default_handler

if __name__ == "__main__":
    ub_webcontent_app.run()

# If app is started via gunicorn
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.logger')
    ub_webcontent_app.logger.handlers = gunicorn_logger.handlers
    ub_webcontent_app.logger.setLevel(gunicorn_logger.level)
    ub_webcontent_app.logger.removeHandler(default_handler)
    ub_webcontent_app.logger.info('Starting production server')
